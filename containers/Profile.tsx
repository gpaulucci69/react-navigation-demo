import React, { useEffect, useState } from 'react'
import { SafeAreaView, Text, TextInput, Button } from "react-native"
import { connect } from 'react-redux'
import { getUser, updateName } from '../redux/appReducer'

const Profile = ({ user, loggedInUser, updateLoggedInUserName }) => {
    const { name } = user(loggedInUser)
    return (
        <SafeAreaView style={{ flex: 1, justifyContent: 'center', margin: 10 }}>
            <Text
                style={{
                    marginVertical: 10
                }}
            >Profile Screen: {name}</Text>
            <TextInput
                placeholder={"Enter User name"}
                value={name}
                onChangeText={(text) => updateLoggedInUserName(text)}
                style={{
                    borderWidth: 1,
                    borderColor: '#43B02A',
                    padding: 10,
                    width: '100%'
                }} />
        </SafeAreaView>
    )
}


const mapStateToProps = (state) => {
    return {
        loggedInUser: state.loggedInUserId,
        user: (id: string | number) => getUser(state, id),
        wrongUser: state.loggedInUser
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateLoggedInUserName: (value: string) => dispatch(updateName(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)