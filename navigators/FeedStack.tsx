import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'
import UserFeedScreen from '../containers/UserFeedScreen'
import UserScreen from '../containers/UserScreen'

const Stack = createStackNavigator()

const FeedStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="UsersFeed" component={UserFeedScreen} />
            <Stack.Screen name="User" component={UserScreen} />
        </Stack.Navigator>
    )
}

export default FeedStack